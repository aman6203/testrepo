<footer>
			<div class="footer-top">
				<div class="container">
					<div class="foo-grids">
						<div class="col-md-3 footer-grid">
							<h4 class="footer-head">Who We Are</h4>
							<p>MEGA APP is a mobile app discovery service dedicated to helping consumers and businesses find iOS and Android applications.</p>
							<!--<p>The point of using Lorem Ipsum is that it has a more-or-less normal letters, as opposed to using 'Content here.</p>-->
						</div>
						<div class="col-md-3 footer-grid">
							
							<ul>
							   <li><img src="apps.jpg"alt="Smiley face" ></li>							
								
								
							</ul>
						</div>
						<div class="col-md-3 footer-grid">
							<ul>
							   <li><img src="12.jpg"alt="Smiley face" ></li>							
								
								
							</ul>
						</div>
						<div class="col-md-3 footer-grid">
							<h4 class="footer-head">Contact Us</h4>
							
							<address>
								<ul class="location">
									<li><span class="glyphicon glyphicon-map-marker"></span></li>
									<li>Plot No.E-110 Industrial Area, Phase 7, Mohali</li>
									<div class="clearfix"></div>
								</ul>	
								<ul class="location">
									<li><span class="glyphicon glyphicon-earphone"></span></li>
									<li>+91-70874-25488</li>
									<div class="clearfix"></div>
								</ul>	
								<ul class="location">
									<li><span class="glyphicon glyphicon-envelope"></span></li>
									<li><a href="mailto:info@sachtechsolution.com">info@sachtechsolution.com</a></li>
									<div class="clearfix"></div>
								</ul>						
							</address>
						</div>
						<div class="clearfix"></div>
					</div>						
				</div>	
			</div>	
			<div class="footer-bottom text-center">
			<div class="container">
				<div class="footer-logo">
					<a href="index.html"><span>App</span>store</a>
				</div>
				<div class="footer-social-icons">
				<ul>
						<li><a class="facebook" href="https://www.facebook.com"><span>Facebook</span></a></li>
						<li><a class="twitter" href="https://www.twitter.com"><span>Twitter</span></a></li>
						<li><a class="flickr" href="https://www.flickr.com"><span>Flickr</span></a></li>
						<li><a class="googleplus" href="https://www.googleplus.com"><span>Google+</span></a></li>
						<li><a class="dribbble" href="https://www.dribbble.com"><span>Dribbble</span></a></li>
					</ul>
				</div>
				<div class="copyrights">
					<p> © 2017 Appstore. All Rights Reserved | Design &amp; Develop by  <a href="http://sachtechsolution.com/"> Amandeep Kaur</a></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		</footer>